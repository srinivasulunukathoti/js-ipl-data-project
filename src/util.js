const convertor = require('csvtojson');


function csvToJSON(path) {
    return convertor().fromFile(path).then((data) => {
        return data;
    });
};


module.exports = csvToJSON;