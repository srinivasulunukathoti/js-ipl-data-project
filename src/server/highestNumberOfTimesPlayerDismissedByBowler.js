const { log, count } = require("console");
const csvToJSON = require("../util");

const fs = require('fs');
const deliveriesPath = '../data/deliveries.csv';

function highestNumberOfTimesPlayerDismissedByAnotherPlayer(batter) {
try {
    csvToJSON(deliveriesPath).then((deliveriesData)=>{
        // Initializing the empty object to store data
        const playerDismissedByAnotherPlayer = deliveriesData.reduce((accumulator, deliveries) =>{
            if (deliveries.player_dismissed === batter) {
                if (!accumulator[deliveries.player_dismissed]) {
                    accumulator[deliveries.player_dismissed] ={};
                } else {
                    if (!accumulator[deliveries.player_dismissed][deliveries.bowler]) {
                        accumulator[deliveries.player_dismissed][deliveries.bowler]=1;
                    } else {
                        accumulator[deliveries.player_dismissed][deliveries.bowler]++;
                    }
                }
            }
            return accumulator; 
        }, {});
        let result =[];
        let count =1;
         result = Object.keys(playerDismissedByAnotherPlayer).reduce((accumulator, batter)=>{
            return Object.keys(playerDismissedByAnotherPlayer[batter]).reduce((acc , bowler)=>{
                if (count == playerDismissedByAnotherPlayer[batter][bowler]) {
                    count =playerDismissedByAnotherPlayer[batter];
                    accumulator.push(bowler);
                } else if(count < playerDismissedByAnotherPlayer[batter][bowler]){
                    count =playerDismissedByAnotherPlayer[batter][bowler];
                    accumulator = [];
                    accumulator.push(bowler , count);
                }
               
                return accumulator;
            },{});
            
        },{});
           
           
         console.log(result);
         //providing the path 
         fs.writeFile('../public/output/highest_number_of_times_player_dismissed_by_bowler.json',JSON.stringify(result),(err)=>{
            if (err) {
                console.log(err);
            } else {
                console.log("successfully completed problem");
            }
         });
        
    });
} catch (error) {
    console.log("error");
}
};
highestNumberOfTimesPlayerDismissedByAnotherPlayer("DA Warner");