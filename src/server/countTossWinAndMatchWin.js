const csvToJSON = require("../util");

const fs = require('fs');
const matchPath = '../data/matches.csv';

function countTossWinAndMatchWin() {
    try {
        csvToJSON(matchPath).then((matchData) => {
            
            const countTossAndMatchWin =matchData.reduce((accumulator,matches) => {
                if (matches.toss_winner === matches.winner) {
                    if (accumulator[matches.toss_winner]) {
                        accumulator[matches.toss_winner]++
                    } else {
                        accumulator[matches.toss_winner]=1;
                    }
                }
                return accumulator;
            });
           
            console.log(countTossAndMatchWin);
            fs.writeFile('../public/output/count_toss_win_and_match_win.json',JSON.stringify(countTossAndMatchWin),(err)=>{
                if (err) {
                    console.log(err);
                } else {
                    console.log("successfully getting data");
                }
            })
            
        });
    } catch (error) {
        console.log("error");
    }
};
countTossWinAndMatchWin();