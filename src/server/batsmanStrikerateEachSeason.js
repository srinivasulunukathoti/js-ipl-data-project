const csvToJSON = require("../util");

const fs = require('fs');

const matchPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';

function batsmanStrikerateEachSeason(batterName) {
try {
    //passsing the matchPath.csv file and getting the data as object array.
    csvToJSON(matchPath).then((matchdata) =>{
        csvToJSON(deliveriesPath).then((deliveriesData)=>{
            //creating object to store data
            const matchDetails = matchdata.filter(( match) => {
                return match.id && match.season;
            });
            const strikerateEachSeason = matchDetails.reduce((process , match) =>{
                const deliveriesDetails = deliveriesData.filter((delivery) =>{
                    return delivery.batsman === batterName && delivery.match_id==match.id;
                });
                const runsAndBalls = deliveriesDetails.reduce((accumulator, deliveries) => {
                    const year = match.season;
                    if (!accumulator[year]) {
                        accumulator[year] = { batter: batterName, run: 0, balls: 0 };
                    }
                    accumulator[year].run += Number((deliveries.total_runs) - (deliveries.legbye_runs));
                    accumulator[year].balls++;
                    return accumulator;
                }, {});
            
                return Object.keys(runsAndBalls).reduce((accumulator, year) => {
                    if (!accumulator[year]) {
                        accumulator[year] = runsAndBalls[year];
                    } else {
                        accumulator[year].run += runsAndBalls[year].run;
                        accumulator[year].balls += runsAndBalls[year].balls;
                    }
                    return accumulator;
                }, process);
            },{});
            const strikerate = Object.keys(strikerateEachSeason).reduce((accumulator, season) => {
                const runs = strikerateEachSeason[season].run;
                const balls = strikerateEachSeason[season].balls;
                const strikerate = (runs / balls) * 100;
                const batsman = strikerateEachSeason[season].batter;
            
                accumulator[season] = { batsman: batsman, strikerate: strikerate };
                return accumulator;
            }, {});
            
            console.log(strikerate);
           
            fs.writeFile('../public/output/batsman_strikerate_each_season.json',JSON.stringify(strikerate),(err)=>{
                if (err) {
                    console.log(err);
                }else{
                    console.log("successfully getting the output");
                }
            })
        });
    });
} catch (error) {
    console.log("error");
}
};
batsmanStrikerateEachSeason("S Dhawan")