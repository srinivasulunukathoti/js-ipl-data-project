const csvToJSON = require("../util");

const fs = require('fs');
const deliveriesPath = '../data/deliveries.csv';

function bowlerBestEconomyInSuperOver() {
try {
    csvToJSON(deliveriesPath).then((deliveriesData)=>{
        
        const economicalBowlerInSuperOver =deliveriesData.reduce((accumulator,deliveries)=>{
            if (deliveries.is_super_over ==1) {
                if (!accumulator[[deliveries.bowler]]) {
                    accumulator[deliveries.bowler]={runs:0,balls:0};
                } else {
                    accumulator[deliveries.bowler].runs +=Number([deliveries.total_runs]-[deliveries.legbye_runs]);
                    accumulator[deliveries.bowler].balls++;
                }
            }
            return accumulator;
        },{});
        
        const result = Object.keys(economicalBowlerInSuperOver).reduce((accumulator,bowler) =>{
            let runs = economicalBowlerInSuperOver[bowler].runs;
            let balls = economicalBowlerInSuperOver[bowler].balls;
            let overs = balls/6;
            let economy = runs/overs;
            accumulator[bowler]={economy:economy};
            return accumulator;
        },{});
        let count =20;
        let bestEconomy = Object.keys(result).reduce((accumulator,bowler)=>{
            if (count<result[bowler].economy) {
                accumulator.push(bowler);
                count= result[bowler].economy
            }else{
                count = result[bowler].economy;
                accumulator=[];
                accumulator.push(bowler ,count);
            }
            return accumulator;
        },{});
        console.log(bestEconomy);
        fs.writeFile('../public/output/best_economyIn_super_over.json',JSON.stringify(bestEconomy),(err)=>{
            if (err) {
                console.log(err);
            }else{
                console.log("Problem has been solved");
            }
        })
    });
} catch (error) {
    console.log("error");
}
};
bowlerBestEconomyInSuperOver();