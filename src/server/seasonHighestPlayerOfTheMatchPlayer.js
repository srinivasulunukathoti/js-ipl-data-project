const { match } = require("assert");
const csvToJSON = require("../util");
const fs = require('fs')
const matchPath = '../data/matches.csv';

function seasonHighestPlayerOfTheMatchPlayer() {
try {
    const playerofmatch=csvToJSON(matchPath).then((matchData) => {
         // Initializing the empty object to store data
        const playerOfTheMatch =matchData.reduce((accumulator,matches) =>{
            if (!accumulator[matches.season]) {
                accumulator[matches.season]={};
            } else {
                if (accumulator[matches.season][matches.player_of_match]) {
                    accumulator[matches.season][matches.player_of_match]++;
                } else {
                    accumulator[matches.season][matches.player_of_match]=1;
                }
            }
            return accumulator;
        },{});
        const result =Object.keys(playerOfTheMatch).reduce((accumulator,season) => {
            let count =0;
            let players = Object.keys(playerOfTheMatch[season]).reduce((acc,name) => {
                if (count == playerOfTheMatch[season][name]) {
                    acc.push(name);
                }
                else if (count <playerOfTheMatch[season][name]) {
                    count = playerOfTheMatch[season][name];
                    acc =[];
                    acc.push(name);
                }
                return acc;
            },{});
            accumulator[season]={players:players, 'man of the matches':count};
            return accumulator;
        },{});
        console.log(result);
        
        

            fs.writeFile('../public/output/season_highest_playerofmatch_player.json',JSON.stringify(result),(err) =>{
                if (err) {
                    console.log(err);
                } else {
                    console.log("successfully completed the problem");
                }
            })
        }); 
} catch (error) {
    console.log("error");
}
};
seasonHighestPlayerOfTheMatchPlayer();