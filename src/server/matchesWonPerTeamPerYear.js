const csvToJSON = require('../util');

const fs = require('fs');

function matchesWonPerTeamPerYear(params) {
    try {
        csvToJSON('../data/matches.csv').then(data => {
            const countMatchesWon = 
                data.reduce((acc ,element)=>{
                    if(!acc[element.season]){
                        acc[element.season]={};
                    }else{
                        if (acc[element.season][element.winner]) {
                            acc[element.season][element.winner]++;
                        } else {
                            acc[element.season][element.winner]=1;
                        }
                    }
                    return acc;
                },{});
           

            console.log(countMatchesWon);
            fs.writeFile('../public/output/matches_won_per_team_per_year.json',JSON.stringify(countMatchesWon),(err)=>{
                if (err) {
                    console.log(err);
                } else {
                    console.log("successfully solved the problem");
                }
            })
        });
    } catch (error) {
        console.log("error");
    }
};

matchesWonPerTeamPerYear();

