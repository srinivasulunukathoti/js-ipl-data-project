const { match } = require('assert');
const csvToJSON = require('../util');

const fs = require('fs');
 const deliveriesPath = '../data/deliveries.csv';


 function extraRunsTeamConceded2016(year) {
    try {
        csvToJSON('../data/matches.csv').then((data)=>{
            
            csvToJSON(deliveriesPath).then((delivery) =>{
                const matchesFilter = data.filter((match) =>{
                    return match.season==year;
                });
                const extraRuns =matchesFilter.reduce((accumulatorPrev ,match)=>{
                    const deliveriesFilter = delivery.filter((delivaryball) =>{
                        return delivaryball.match_id == match.id;
                    });
                    const extraRunsTeamsThisMatch =deliveriesFilter.reduce((accumulator,deliveries) =>{
                        if (!accumulator[deliveries.bowling_team]) {
                            accumulator[deliveries.bowling_team] = Number(deliveries.extra_runs);
                        } else {
                            accumulator[deliveries.bowling_team] += Number(deliveries.extra_runs);
                        }
                        return accumulator;
                    },{});
                    return Object.keys(extraRunsTeamsThisMatch).reduce((accumulator,teams) =>{
                        if (!accumulator[teams]) {
                            accumulator[teams] = extraRunsTeamsThisMatch[teams];
                        } else {
                            accumulator[teams] += extraRunsTeamsThisMatch[teams];
                        }
                        return accumulator;
                    },accumulatorPrev);
                },{});
                
                 console.log(extraRuns);
                fs.writeFile('../public/output/extra_runs_team_conceded_2016.json',JSON.stringify(extraRuns),(err)=>{
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successfully completed problem");
                    }
                })
            });
            
        });
    } catch (error) {
        console.log("error");
    }
    
 };

 extraRunsTeamConceded2016(2016);