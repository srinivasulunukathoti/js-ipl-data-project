
const csvToJSON = require('../util');

const fs = require('fs'); 
const matchPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';

function top10EconomicalBowlersIn2015(year) {
   try {
    csvToJSON(matchPath).then((matchData) =>{
       
        csvToJSON(deliveriesPath).then((deliveriesData)=>{
             const filterMatches = matchData.filter((match) =>{
            return match.season == year;
        });
        const economicalBowler = filterMatches.reduce((previous,match) =>{
            const filterDeliveries = deliveriesData.filter((delivery) =>{
                return delivery.match_id == match.id;
            });
            const economicalBowlersThisMatch = filterDeliveries.reduce((accumulator, deliveries) =>{
                if (!accumulator[deliveries.bowler]) {
                    accumulator[deliveries.bowler]={runs :0, balls:0};
                } else {
                    accumulator[deliveries.bowler].runs +=Number(deliveries.total_runs)-Number(deliveries.legbye_runs);
                    accumulator[deliveries.bowler].balls++;
                }
                return accumulator;
            },{});
            return Object.keys(economicalBowlersThisMatch).reduce((accumulator ,bowler) =>{
                if (!accumulator[bowler]) {
                    accumulator[bowler] = economicalBowlersThisMatch[bowler];
                } else {
                    accumulator[bowler].runs += economicalBowlersThisMatch[bowler].runs;
                    accumulator[bowler].balls += economicalBowlersThisMatch[bowler].balls;
                }
                return accumulator;
            },previous);
        },{});
         const result =Object.keys(economicalBowler).reduce((accumulator,bowler) =>{
            const overs = (economicalBowler[bowler].balls)/6;
            const economy = (economicalBowler[bowler].runs)/overs;
            accumulator[bowler]={economy:economy};
            return accumulator;
         },{});
        
         const top10EconomicalBowlers = Object.entries(result).sort(([, economy1], [, economy2]) => economy1.economy - economy2.economy).slice(0,10);
         console.log(top10EconomicalBowlers);
          
            fs.writeFile('../public/output/top10_economical_bowlersIn_2015.json',JSON.stringify(top10EconomicalBowlers),(err)=>{
                if (err) {
                    console.log(err);
                } else {
                    console.log("successfully completed the problem");
                }
            })
            
            
        });
        
        });
   } catch (error) {
    console.log("error");
   }
            
    };
    top10EconomicalBowlersIn2015(2015);