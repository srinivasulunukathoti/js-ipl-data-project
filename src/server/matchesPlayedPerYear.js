const { error, log } = require('console');
const csvToJSON = require('../util');
 const fs =require('fs')
function matchesPlayedPerYear() {
try {
    csvToJSON('../data/matches.csv').then((data) => {
         let count = {};
        count=data.reduce((acc , element)=>{
            if (acc[element.season]) {
                acc[element.season]++;
            } else {
                acc[element.season]=1;
            }
            return acc;
        },{})
         fs.writeFile('../public/output/matches_per_year.json',JSON.stringify(count),(err) =>{
            if (err) {
                console.log(err);
            } else {
                console.log("problem has been solved");
            } 
         })

    });
} catch (error) {
    console.log("error");
}
};

matchesPlayedPerYear();